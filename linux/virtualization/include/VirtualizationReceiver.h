/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-FileCopyrightText: Politecnico Di Milano
 */

#ifndef EDDIE_VIRTUALIZATION_RECEIVER_H
#define EDDIE_VIRTUALIZATION_RECEIVER_H

#include <glib.h>
#include <gio/gio.h>

#include "EddieEndpoint.h"

class VirtualizationReceiver {
private:
    static EddieEndpoint *eddie_endpoint;
    static std::vector<EddieResource*> resources;
    static GDBusNodeInfo *introspection_data;
    static const GDBusInterfaceVTable interface_vtable;

    static void
    handle_method_call (GDBusConnection         *connection,
                        const gchar             *sender,
                        const gchar             *object_path,
                        const gchar             *interface_name,
                        const gchar             *method_name,
                        GVariant                *parameters,
                        GDBusMethodInvocation   *invocation,
                        gpointer                user_data);

    static void
    on_bus_acquired(GDBusConnection *connection, const gchar *name, gpointer user_data);

    static void
    on_name_acquired(GDBusConnection *connection, const gchar *name, gpointer user_data);

    static void
    on_name_lost(GDBusConnection *connection, const gchar *name, gpointer user_data);

public:
    VirtualizationReceiver(const std::string& ip = "", const std::string& port = "5683");

    static EddieEndpoint *communication_layer();

    int run(std::vector<std::string> uris, std::vector<std::string> attributes);

    static void update_resources();
};

#endif //EDDIE_VIRTUALIZATION_RECEIVER_H
