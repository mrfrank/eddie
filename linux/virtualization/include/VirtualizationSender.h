/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-FileCopyrightText: Politecnico Di Milano
 */

#ifndef EDDIE_VIRTUALIZATION_SENDER_H
#define EDDIE_VIRTUALIZATION_SENDER_H

#include <glib.h>
#include <unordered_map>
#include <string>

guint connect();

void disconnect(guint watcher_id);

std::string send_message(const std::unordered_map<std::string, std::string> &parameters);

void run();

#endif //EDDIE_VIRTUALIZATION_SENDER_H
