/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-FileCopyrightText: Politecnico Di Milano
 */

#include <thread>
#include <unordered_map>

#include "VirtualizationSender.h"

int main() {
    std::unordered_map<std::string, std::string> parameters = {
            {"resource_consumption", "high"},
            {"method", "GET"},
            {"payload", ""}
    };

    guint watcher_id = connect();

    std::thread main([]() {
        run();
    });

    auto answer = send_message(parameters);
    printf("%s\n", answer.c_str());

    disconnect(watcher_id);

    main.join();
}
