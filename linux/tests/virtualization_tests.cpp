/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-FileCopyrightText: Politecnico Di Milano
 */
#include <gtest/gtest.h>
#include <thread>
#include <unordered_map>

#include "VirtualizationSender.h"

TEST(Communication, Virtualization_Sender) {
    guint watcher_id = connect();

    std::thread main([]() {
        run();
    });

    std::unordered_map<std::string, std::string> parameters = {
            {"method", "POST"},
            {"payload", "100"}
    };
    auto answer = send_message(parameters);
    EXPECT_EQ(answer, "");

    parameters = {
            {"method", "GET"},
            {"payload", ""}
    };
    answer = send_message(parameters);
    EXPECT_EQ(answer, "100");

    disconnect(watcher_id);

    main.join();
}