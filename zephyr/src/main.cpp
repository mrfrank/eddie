/*
 * Copyright (c) 2022 Huawei Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include "EddieEndpoint.h"
#include "eddie.h"
#include <zephyr/usb/usb_device.h>

#include <logging/log.h>
LOG_MODULE_REGISTER(main, LOG_LEVEL_DBG);

#ifdef CONFIG_NET_L2_OPENTHREAD
	#include <zephyr/net/openthread.h>

	int connected = false;

	void ot_state_changed_callback(otChangedFlags aFlags, void *aContext) {
		if(aFlags == OT_CHANGED_THREAD_NETDATA)
			connected = true;
	}

	void wait_for_networking() {
		openthread_set_state_changed_cb(ot_state_changed_callback);

		while(!connected) k_sleep(K_SECONDS(1));
	}
#else
	void wait_for_networking() {
		k_sleep(K_SECONDS(1));
	}
#endif

class EddieLamp : public EddieResource {
private:
	const char * const core_path[2] = { "zephyr-lamp", NULL };
	const char * const core_attributes[3] = { "rt=eddie.lamp", "ct=40", NULL };

	int lamp_status = 0;

public:

	message_t render_get(request_t &request) {
		message_t response;
        response.data = lamp_status == 0 ? "off" : "on";
		response.status_code = CONTENT;
		return response;
	}

	message_t render_post(request_t &request) {
		message_t response;
		std::string request_payload_str = std::string((const char*)request.data, request.data_length);
		if (request_payload_str.compare("on") == 0) lamp_status = 1;
		else if (request_payload_str.compare("off") ==  0) lamp_status = 0;
		else {
			response.status_code = BAD_REQUEST;
			return response;
		}
		response.status_code = CHANGED;
		return response;
	}

	message_t render_put(request_t &request) {
		return render_post(request);
	}

	const char * const* get_path() {
		return this->core_path;
	}

	const char * const* get_attributes() {
		return this->core_attributes;
	}
};

class EddieTemperature : public EddieResource {
private:
	const char * const core_path[2] = { "zephyr-temp", NULL };
	const char * const core_attributes[3] = { "rt=eddie.temp", "ct=40", NULL };

public:

	message_t render_get(request_t &request) {
		message_t response;
        response.data = "very hot";
		response.status_code = CONTENT;
		return response;
	}

	const char * const* get_path() {
		return this->core_path;
	}

	const char * const* get_attributes() {
		return this->core_attributes;
	}
};

static void start_eddie() {
	EddieEndpoint *node = new EddieEndpoint();
	EddieLamp lamp_resource;
	EddieTemperature temperature_resource;

	if (node->discover_rd() != 0) {
		LOG_ERR("Resource directory not found");
		return;
	}

	node->add_resource(&lamp_resource);
	node->add_resource(&temperature_resource);
	node->publish_resources();

	std::vector<Link> discovered_resources = node->get_resources_from_rd();

	// print discovered resources
	for (Link link: discovered_resources) {
		LOG_DBG("discovered resource: host=%s port=%s path=%s", link.host.c_str(), link.port.c_str(), link.path.c_str());
	}

	// send a get request to each discovered resource
	CoapClient client;
	for (Link link: discovered_resources) {
		// ignore my resources
		if (link.path == "zephyr-lamp" || link.path == "zephyr-temp")
			continue;

		request_t get_request;
		get_request.dst_host = link.host.c_str();
		get_request.dst_port = link.port.c_str();
		get_request.method = GET;
		get_request.path = link.path.c_str();
		get_request.content_format = 40;

		message_t get_response = client.send_message_and_wait_response(get_request);
		LOG_DBG("resource host=%s port=%s path=%s status=%s", 
								link.host.c_str(), 
								link.port.c_str(), 
								link.path.c_str(), 
								get_response.data.c_str());
	}
	client.close_coap_client();

	node->start_server(true);
	node->stop_server();
	delete node;
}

int main(void)
{
	if (usb_enable(NULL)) {
		return -1;
	}
	
	wait_for_networking();
	start_eddie();
	
	return 0;
}
