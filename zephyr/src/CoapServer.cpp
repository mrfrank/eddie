/*
 * Copyright (c) 2022 Huawei Inc.
 * Copyright (c) 2018 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <logging/log.h>
LOG_MODULE_REGISTER(coap_server, LOG_LEVEL_DBG);

#include "CoapServer.h"

#include <zephyr/net/coap.h>
#include <zephyr/net/coap_link_format.h>

extern "C" {
	#include <net_private.h>
	#include <ipv6.h>
}

K_THREAD_STACK_DEFINE(my_stack_area, 500);


////////////////// PRIVATE METHODS //////////////////////

bool CoapServer::join_coap_multicast_group(uint16_t port)
{
#if defined(CONFIG_NET_IPV6)

	static struct in6_addr my_addr;
	static struct sockaddr_in6 mcast_addr = {
		.sin6_family = AF_INET6,
		.sin6_port = htons(port),
		.sin6_addr = ALL_NODES_LOCAL_COAP_MCAST };
	struct net_if_addr *ifaddr;
	struct net_if *iface;
	int ret;

	iface = net_if_get_default();
	if (!iface) {
		LOG_ERR("Could not get te default interface\n");
		return false;
	}

	if (net_addr_pton(AF_INET6,
			  CONFIG_NET_CONFIG_MY_IPV6_ADDR,
			  &my_addr) < 0) {
		LOG_ERR("Invalid IPv6 address %s",
			CONFIG_NET_CONFIG_MY_IPV6_ADDR);
	}


	ifaddr = net_if_ipv6_addr_add(iface, &my_addr, NET_ADDR_MANUAL, 0);
	if (!ifaddr) {
		LOG_ERR("Could not add unicast address to interface");
		return false;
	}

	ifaddr->addr_state = NET_ADDR_PREFERRED;

	ret = net_ipv6_mld_join(iface, &mcast_addr.sin6_addr);
	if (ret < 0) {
		LOG_ERR("Cannot join %s IPv6 multicast group (%d)",
			net_sprint_ipv6_addr(&mcast_addr.sin6_addr), ret);
		return false;
	}

	LOG_DBG("joined multicast group");

#endif
	return true;
}

int CoapServer::start_coap_server(uint16_t port)
{
	join_coap_multicast_group(port);

#if defined(CONFIG_NET_IPV4)
	struct sockaddr_in addr;
	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);

	sock = socket(addr.sin_family, SOCK_DGRAM, IPPROTO_UDP);
#else
	struct sockaddr_in6 addr;
	memset(&addr, 0, sizeof(addr));
	addr.sin6_family = AF_INET6;
	addr.sin6_port = htons(port);

	sock = socket(addr.sin6_family, SOCK_DGRAM, IPPROTO_UDP);
#endif

	if (sock < 0) {
		LOG_ERR("Failed to create UDP socket %d", errno);
		return -errno;
	}

	int r = bind(sock, (struct sockaddr *)&addr, sizeof(addr));
	if (r < 0) {
		LOG_ERR("Failed to bind UDP socket %d", errno);
		return -errno;
	}

	return 0;
}

int CoapServer::process_coap_request(uint8_t *data, uint16_t data_len,
				 struct sockaddr *client_addr,
				 socklen_t client_addr_len) {
	struct coap_packet request;
	struct coap_pending *pending;
	struct coap_option options[16] = { 0 };
	uint8_t opt_num = 16U;
	uint8_t type;
	int r;

	r = coap_packet_parse(&request, data, data_len, options, opt_num);
	if (r < 0) {
		LOG_ERR("Invalid data received (%d)\n", r);
		return -1;
	}

	type = coap_header_get_type(&request);

	pending = coap_pending_received(&request, pendings, NUM_PENDINGS);
	if (pending) {
		/* Clear CoAP pending request */
		if (type == COAP_TYPE_ACK || type == COAP_TYPE_RESET) {
			k_free(pending->data);
			coap_pending_clear(pending);
		}
	}
	else {
		r = coap_handle_request(&request, this->site->get_resources_arr(), options, 
					opt_num, client_addr, client_addr_len);
		if (r < 0) {
			LOG_WRN("No handler for such request (%d)\n", r);
		}
	}
	return r;
}

int CoapServer::process_client_request(void)
{
	int received;
	struct sockaddr client_addr;
	socklen_t client_addr_len;
	uint8_t request[MAX_COAP_MSG_LEN];

	client_addr_len = sizeof(client_addr);
	received = recvfrom(sock, request, sizeof(request), 0,
				&client_addr, &client_addr_len);
	if (received < 0) {
		LOG_ERR("Connection error %d", errno);
		return -errno;
	}

	net_hexdump("Received request", request, received);

	return process_coap_request(request, received, &client_addr,
					client_addr_len);
}

///////////// PUBLIC METHODS ////////////

CoapServer::CoapServer(const std::string& port) {
	int r = start_coap_server(std::stoul(port));
	if (r<0) LOG_ERR("Could not start coap server");
	this->client = new CoapClient(this->sock);

	this->site = new CoapSite();
	CoapSite::set_sock(this->sock);
}

CoapServer::~CoapServer() {
	delete this->client;
	delete this->site;
}

CoapClient* CoapServer::get_client() {
	return client;
}

int CoapServer::run() {
	while (!get_quit()) {
		if (process_client_request() < 0) {
			LOG_ERR("Error processing client request");
		}
		LOG_DBG("Request handled");
	}
	return 0;
}

int CoapServer::start_server() {
	LOG_DBG("[CoapServer] starting server");

	auto server_task = [](void * server, void *, void *) {
		((CoapServer*)server)->run();
	};
	// run server on a separate thread
	this->server_thread = k_thread_create(&my_thread_data, my_stack_area,
									K_THREAD_STACK_SIZEOF(my_stack_area),
									server_task,
									this, NULL, NULL,
									5, 0, K_NO_WAIT);
	LOG_DBG("server started");
	return 0;
}

int CoapServer::stop_server() {
	set_quit(true);
	k_thread_join(this->server_thread, K_FOREVER);
	LOG_DBG("server stopped");
	return 0;
}

int CoapServer::add_resource(EddieResource *resource) {
	return this->site->add_resource(resource);
}

std::string CoapServer::get_resources_in_linkformat() {
	return this->site->get_resources_in_linkformat();
}

bool CoapServer::get_quit() const {
    return quit;
}

void CoapServer::set_quit(bool val) {
    quit = val;
}