/*
 * Copyright (c) 2022 Huawei Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#include "CoapSite.h"
#include "eddie.h"
#include "CoapUtils.h"

#include <logging/log.h>
LOG_MODULE_REGISTER(site, LOG_LEVEL_DBG);

extern "C" {
	#include <net_private.h>
}

#include <zephyr/net/coap_link_format.h>
#include <functional>

#define MAX_COAP_MSG_LEN 256

////////////////////////////////// PRIVATE /////////////////////////////////

int CoapSite::sock;

int CoapSite::send_coap_reply(struct coap_packet *cpkt,
			   const struct sockaddr *addr,
			   socklen_t addr_len)
{
	int r;

	net_hexdump("Sending response", cpkt->data, cpkt->offset);

	r = sendto(sock, cpkt->data, cpkt->offset, 0, addr, addr_len);
	if (r < 0) {
		LOG_ERR("Failed to send %d", errno);
		r = -errno;
	}

	return r;
}

int CoapSite::well_known_core_get(struct coap_resource *resource,
			       struct coap_packet *request,
			       struct sockaddr *addr, socklen_t addr_len)
{
	struct coap_packet response;
	uint8_t *data;
	int r;

	data = (uint8_t *)k_malloc(MAX_COAP_MSG_LEN);
	if (!data) {
		return -ENOMEM;
	}

	r = coap_well_known_core_get(resource, request, &response,
				     data, MAX_COAP_MSG_LEN);
	if (r < 0) {
		goto end;
	}

	r = send_coap_reply(&response, addr, addr_len);

end:
	k_free(data);

	return r;
}

int CoapSite::send_response(message_t response_message, 
						struct coap_packet *request, 
						struct sockaddr *addr, 
						socklen_t addr_len) {
	struct coap_packet response;
	uint8_t token[COAP_TOKEN_MAX_LEN];
	uint8_t *data;
	uint16_t id;
	uint8_t tkl;
	int r;
	
	// make response packet
	id = coap_header_get_id(request);
	tkl = coap_header_get_token(request, token);

	data = (uint8_t *)k_malloc(MAX_COAP_MSG_LEN);
	if (!data) {
		return -ENOMEM;
	}

	r = coap_packet_init(&response, data, MAX_COAP_MSG_LEN,
				COAP_VERSION_1, COAP_TYPE_ACK, tkl, token,
				response_message.status_code, id);
	if (r < 0) {
		r = -EINVAL;
		goto end;
	}

	if (response_message.data.size() > 0) {
		r = coap_packet_append_payload_marker(&response);
		if (r < 0) {
			r = -EINVAL;
			goto end;
		}

		r = coap_packet_append_payload(&response, 
										(const uint8_t*)response_message.data.c_str(), 
										response_message.data.length());
		if (r < 0) {
			r = -EINVAL;
			goto end;
		}
	}

	r = send_coap_reply(&response, addr, addr_len);

end:
	k_free(data);
	return r;
}

int CoapSite::resource_handler(struct coap_resource *resource,
		    struct coap_packet *request,
		    struct sockaddr *addr, socklen_t addr_len)
{
	uint16_t payload_length;
	const uint8_t * payload = coap_packet_get_payload(request, &payload_length);
	uint8_t coap_method = coap_header_get_code(request);
	request_t message_request;
	message_request.data = payload;
	message_request.data_length = payload_length;
	message_request.method = coap_method_to_method(coap_method);

	coap_core_metadata *metadata = static_cast<coap_core_metadata*>(resource->user_data);
	EddieResource *eddie_resource = static_cast<EddieResource*>(metadata->user_data);

	message_t response_message = eddie_resource->request_handler(message_request);
	return send_response(response_message, request, addr, addr_len);
}

////////////////////////////////// PUBLIC /////////////////////////////////

CoapSite::CoapSite() {
    coap_resource well_known_core_resource { 
        .get = well_known_core_get,
	    .path = COAP_WELL_KNOWN_CORE_PATH,
	};
    resources.push_back(well_known_core_resource);

    // the coap library wants the array of resources terminated by a NULL resource
    coap_resource empty_resource {};
    resources.push_back(empty_resource);
}

int CoapSite::add_resource(EddieResource *resource) {
	
    coap_resource new_resource { 
        .get = resource_handler,
		.post = resource_handler,
		.put = resource_handler,
		.del = resource_handler,
	    .path = resource->get_path(),
	};

	new_resource.user_data = new coap_core_metadata {
		.attributes = resource->get_attributes(),
		.user_data = resource // storing EddieResource ptr so that it can be accessed by the request handlers
	};

    resources.pop_back(); // pop NULL delimiter

    resources.push_back(new_resource);

    // the coap library wants the array of resources terminated by a NULL resource
    coap_resource empty_resource {};
    resources.push_back(empty_resource);

    return 0;
}

void CoapSite::set_sock(int sock) {
	CoapSite::sock = sock;
}

coap_resource* CoapSite::get_resources_arr() {
    return &resources[0];
}


std::string CoapSite::get_resources_in_linkformat() {

	struct coap_packet request;
	struct coap_packet response;
	int r;

	uint8_t* buffer = (uint8_t *)k_malloc(MAX_COAP_MSG_LEN);
	if (!buffer) {
		LOG_ERR("[CoapSite::get_resources_in_linkformat] ENOMEM");
		return "";
	}
	
	r = coap_packet_init(&request, buffer, MAX_COAP_MSG_LEN, 
				 COAP_VERSION_1, COAP_TYPE_CON,
			     COAP_TOKEN_MAX_LEN, coap_next_token(),
			     COAP_METHOD_POST, coap_next_id());
	if (r < 0) {
		LOG_ERR("[CoapSite::get_resources_in_linkformat] Error initializing packet");
		return "";
	}

	r = coap_well_known_core_get(get_resources_arr(), &request, &response,
				     buffer, MAX_COAP_MSG_LEN);
	if (r < 0) {
		LOG_ERR("[CoapSite::get_resources_in_linkformat] coap_well_known_core_get error");
		return "";
	}

	uint16_t payload_length;
	const uint8_t * payload = coap_packet_get_payload(&response, &payload_length);
	if (payload) {
		net_hexdump("Publish resources Payload", payload, payload_length);
	}

	return std::string((const char*)payload, (size_t)payload_length);
}