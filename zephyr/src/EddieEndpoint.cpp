/*
 * Copyright (c) 2022 Huawei Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <logging/log.h>
LOG_MODULE_REGISTER(eddie_endpoint, LOG_LEVEL_DBG);

#if defined(CONFIG_NET_IPV4)
	#define COAP_MULTICAST_ADDR "224.0.1.187"
#else
	#define COAP_MULTICAST_ADDR "FF05::FD"
#endif

#include "EddieEndpoint.h"
#include "CoapClient.h"
#include "CoapServer.h"
#include "eddie.h"

#include <net_private.h>

CoapClient* EddieEndpoint::get_client() {
	return get_server()->get_client();
}

CoapServer* EddieEndpoint::get_server() {
	return server;
}

int EddieEndpoint::discover_rd()
{
	request_t get_resources_request;
	get_resources_request.dst_host = COAP_MULTICAST_ADDR;
	get_resources_request.dst_port = "5683";
	get_resources_request.method = GET;
	get_resources_request.path = ".well-known/core";
	get_resources_request.query = "rt=core.rd";
	get_resources_request.confirmable = false;
	get_resources_request.content_format = 40;

	message_t response = get_client()->send_message_and_wait_response(get_resources_request);

	if (response.status_code != CONTENT) return -1;

	resource_directory_ip = response.src_host;
	resource_directory_port = response.src_port;
	LOG_DBG("Resource directory ip: %s \n", this->resource_directory_ip.c_str());

	return 0;
}

int EddieEndpoint::add_resource(EddieResource *resource) {
	return get_server()->add_resource(resource);
}

int EddieEndpoint::start_server(bool blocking) {
	if (blocking)
		return get_server()->run();
	else
		return get_server()->start_server();
}

int EddieEndpoint::stop_server() {
	return get_server()->stop_server();
}

EddieEndpoint::EddieEndpoint(const std::string& port, const std::string& ip) {
    server = new CoapServer(port);
}

EddieEndpoint::~EddieEndpoint() {
    delete server;
}

int EddieEndpoint::publish_resources() {
	std::string payload = get_server()->get_resources_in_linkformat();

	if (payload.empty()) {
		return -1;
	}

	request_t publish_resources_request;
	publish_resources_request.dst_host = this->resource_directory_ip.c_str();
	publish_resources_request.dst_port = "5683"; //TODO: do not hardcode
	publish_resources_request.method = POST;
	publish_resources_request.path = "rd";
	publish_resources_request.query = "ep=zephyrnode&lt=90000"; // TODO: use device id as endpoint name
	publish_resources_request.data = (const uint8_t*)payload.c_str();
	publish_resources_request.data_length = payload.length();
	publish_resources_request.content_format = 40;

	auto response_handler = [](message_t response_message) {
		LOG_DBG("Response: %s", response_message.data.c_str());
	};

	message_t response = get_client()->send_message_and_wait_response(publish_resources_request);
	if (response.status_code != CREATED) {
		LOG_ERR("Error publishing resources, status_code=%d", response.status_code);
	}

	LOG_DBG("Response: %s", response.data.c_str());

	return 0;
}

std::vector<Link> EddieEndpoint::get_resources_from_rd() {
	request_t get_resources_request;
	get_resources_request.dst_host = this->resource_directory_ip.c_str();
	get_resources_request.dst_port = "5683";
	get_resources_request.path = "rd-lookup/res";
	get_resources_request.content_format = 40;

	message_t response = get_client()->send_message_and_wait_response(get_resources_request);

	if (response.status_code != CONTENT) {
		LOG_ERR("Error getting resources from rd, status_code=%d", CONTENT);
		return {};
	}
	return parse_link_format(response.data);
}
