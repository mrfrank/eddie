/*
 * Copyright (c) 2022 Huawei Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __COAPSERVER_H__
#define __COAPSERVER_H__

#include "CoapClient.h"
#include "CoapSite.h"

#define MAX_RETRANSMIT_COUNT 2

#define MAX_COAP_MSG_LEN 256

#define BLOCK_WISE_TRANSFER_SIZE_GET 2048

#define ALL_NODES_LOCAL_COAP_MCAST \
	{ { { 0xff, 0x02, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0xfd } } }

#define NUM_PENDINGS 10


class CoapServer {
private:
    CoapClient *client;

    CoapSite *site;

    k_tid_t server_thread;
    struct k_thread my_thread_data;

    /* CoAP socket fd */
    int sock;

    bool quit = false;

    struct coap_pending pendings[NUM_PENDINGS];

    bool join_coap_multicast_group(uint16_t port);
    
    int start_coap_server(uint16_t port);

    int process_client_request(void);

    int process_coap_request(uint8_t *data, uint16_t data_len,
				 struct sockaddr *client_addr,
				 socklen_t client_addr_len);

    [[nodiscard]] bool get_quit() const;

    void set_quit(bool val);

public:

    /**
     * CoapServer constructor
     * @param port Port number of the server as a string
     */
    CoapServer(const std::string& port);

    ~CoapServer();

    /**
     * The client is the object used to send CoAP requests
     * @return The client object used by CoAP server for the communication
     */
    CoapClient *get_client();

    /**
     * Run the server loop spinning the CoAP I/O processing function. This loop runs until set_quit(true) is called
     * @return 0 on success
     */
    int run();

    /**
     * Start the server loop in a separate thread without blocking.
     * 
     * Note: There are problems when running the server in a separate thread on arduino nano 33 ble, while 
     * it works on qemu_x86:
     * ```
     * <err> os:   Stacking error (context area might be not valid)
     * <err> os:   Data Access Violation
     * ```
     * 
     * @return 0 on success
     */
    int start_server();

    /**
     * Stop the server thread
     * @return 0 on success
     */
    int stop_server();

    /**
     * Add a resource into the server. A resource consists of a path, attributes and REST methods.
     * Adding the resource allows the server to serve it and call the appropriate REST handler
     * whenever it receives a request targeting the served resource.
     * @param resource An EddieResource object containing resource information (e.g. path and attributes) and REST handlers
     * @return 0 on success, -1 on error
     */
    int add_resource(EddieResource *resource);

    /**
     * @brief Get the resources added into the server in link format
     * 
     * @return resources as a link format string
     */
    std::string get_resources_in_linkformat();
};

#endif